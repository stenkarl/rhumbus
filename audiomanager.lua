this = {}

local backgroundMusic = audio.loadStream("audio/rhumbus_theme.mp3")
local backgroundChannel

this.on = function()
  backgroundChannel = audio.play( backgroundMusic, { loops=-1, fadein=500 })
end

this.off = function()
  audio.stop(backgroundChannel)
end

return this