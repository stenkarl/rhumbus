this = {}

local slot = require("slot")

local initialY = 50
local maxY = 170
local tolerance = maxY - 10
local clear
local moving = false

local function reset()
  transition.to( clear, { time=300, y=initialY, transition=easing.inQuad, 
              onComplete= function() moving = false end})    
end

local function trigger()
  --print ("clear trigger")
  slot.reset()
  reset()
end

local function moveDown()
  if moving then return end
  moving = true
  transition.to( clear, { time=400, y=maxY, transition=easing.inQuad, onComplete=trigger})      
end

local function onTouch( event )
  local target = event.target
  --print ("touch "..event.phase)
  if event.phase == "began" then
    target.markY = target.y
  elseif event.phase == "moved" then	
    local y = (event.y - event.yStart) + target.markY
    if y > maxY then 
      y = maxY
    elseif y < initialY then
      y = initialY
    end
    target.y = y
  elseif event.phase == "ended" then
    --print ("ended")
    if target.y < tolerance then
      reset()
    else
      trigger()
    end
  end
    
  return true
end

this.init = function()
  local scale = 0.55
  clear = display.newImageRect("images/ClearButton.png", 199 * scale, 108 * scale)
  clear.x, clear.y = 163, initialY
  clear:addEventListener( "tap", moveDown )
  return clear 
end

return this