this = {}

local initialY = 575
--local image
local step = 20
local minY = 50

local light = {}
local current = 0
local coinHeight = 10
local coinGroup
local leftGear
local moveTime = 500
local curRotation = 0
local goldOnly = true

local function createCoin( which )
  if which ~= "Gold" then
    goldOnly = false
  end
  local image = display.newImageRect("images/CoinStack"..which..".png", 58, coinHeight)
  image:setReferencePoint( display.TopCenterReferencePoint )
  image.y = current * coinHeight
  
  return image
end

local function removeCoins()
  for i=coinGroup.numChildren,1,-1 do
    coinGroup[i]:removeSelf()
    coin = nil
  end
end

local function reset()
  goldOnly = true
  light[0].alpha = 1
  for i = 1,3 do
    light[i].alpha = 0
  end
  current = 0
end

local function flicker(old, img, time, num)
  --print("flicker "..num)
  local nextFlicker = num > 0 and 
              (function() light[img].alpha=0; flicker(old, img, 90, num-1) end) or 
                (function() light[old].alpha=0 end)
  transition.to( light[img], { time=time, alpha=1, onComplete=nextFlicker  } )
end

local function fade(source, dest)
  --transition.dissolve( light[source], light[dest], 3000, 0 ) --{ time=2000, alpha=1 } )
  transition.to( light[dest], { time=1000, alpha=1  } )
end

local function flipNext()
  if current == 0 then
    flicker(current, current + 1, 450, 4)
  elseif current == 10 then
    fade(1, 2)
  elseif current == 20 then
    fade(2, 3)
  end
  current = current + 1
end

local function moveGear()
  curRotation = (curRotation + 20)
  transition.to( leftGear, { time=moveTime, rotation=curRotation })
end

this.move = function( which )
  local name = which < 10 and "Copper" or which < 20 and "Silver" or "Gold"
  coinGroup:insert(createCoin(name))
  local targetY = coinGroup.y - coinHeight
  transition.to( coinGroup, { time=moveTime, y=targetY } )
  flipNext()
  moveGear()
end

this.levelComplete = function()
  --return current > 1
  return current > 39
end

this.retract = function()
  if coinGroup.y ~= initialY then
    local rTime = 2000
    transition.to( coinGroup, { time=rTime, y=initialY, onComplete=removeCoins } ) 
    transition.to( leftGear, { time=rTime, rotation=0 })
    curRotation = 0
  end
  reset()  
end

local function createLight( name )
  local fileName = "images/Lightbulb"..name..".png"
  local img = display.newImageRect(fileName, 110, 110)
  img:setReferencePoint( display.TopCenterReferencePoint )
  img.x, img.y = 73, 70  
  
  img.alpha = 0
  
  return img
end

local function createGear()
  local fileName = "images/LeftGear.png"
  local w = 1173 * 0.4
  leftGear = display.newImageRect(fileName, w, w)
  --leftGear:setReferencePoint( display.TopCenterReferencePoint )
  leftGear.x, leftGear.y = -40, 740  
    
  return leftGear
end

this.initLeftGear = function(group)
  group:insert(createGear())
end

this.init = function(group)
  --image = display.newImageRect("images/CoinStack.png", 190, 552)
  --image:setReferencePoint( display.TopCenterReferencePoint )
  
  light[0] = createLight("Off")
  light[1] = createLight("On1")
  light[2] = createLight("On2")
  light[3] = createLight("On")

  coinGroup = display.newGroup()
  coinGroup.x, coinGroup.y = 70, initialY

  reset()
  
  group:insert(coinGroup)
  group:insert(light[0])
  group:insert(light[1])
  group:insert(light[2])
  group:insert(light[3])
      
  return image
end

this.isLit = function()
  return light[3].alpha == 1
end

this.isGoldOnly = function()
  return goldOnly
end

return this