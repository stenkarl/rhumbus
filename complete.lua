local toggle = require ("toggle")
local numbers = require("numbers")
local storyboard = require("storyboard")

this = {}

local topY = 450
local bottomY = 850
local g

local nextLevel

this.rise = function(nextLevel)
  transition.to(g, {time=2000, y=topY})  
end

local function retract()
  transition.to(g, {time=1500, y=bottomY})  
end

local function doNextLevel()
  retract()
  numbers.dismissAll()
  nextLevel()
end

this.init = function( group, nextLevelCB )
  nextLevel = nextLevelCB
  local scale = 0.6

  local image = display.newImageRect("images/LevelCompleteScreen.png", 759 * scale, 620 * scale)
  image:setReferencePoint( display.TopCenterReferencePoint )
  
  --  image.x, image.y = 420, bottomY
  
  g = display.newGroup()
  --g:setReferencePoint( display.TopCenterReferencePoint )
  g.x, g.y = 423, bottomY  
  g:insert(image)
  
  toggle.createWithSizeNoValue("ContinueButton", "ContinueButtonPressed", 0, -15, 198 * scale , 138 * scale, g, doNextLevel)
  
  group:insert(g)
end

return this