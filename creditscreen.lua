local storyboard = require( "storyboard" )
local audiomanager = require( "audiomanager" )
local toggle = require("toggle")
local scene = storyboard.newScene()
local on, off

local click = audio.loadSound( "audio/click.wav" )

local function musicOn()
  on.isVisible = true
  off.isVisible = false
  audio.play( click )
  audiomanager.on()
end

local function musicOff()
  on.isVisible = false
  off.isVisible = true
  audio.play( click )
  audiomanager.off()
end

local function createImage(name, x, y, w, h, group)
	local img = display.newImageRect("images/"..name..".png", w, h)
	img.x, img.y = x, y
	
	group:insert(img)
	
	return img
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-- display a background image
	local background = 
    display.newImageRect( "images/NumberRhumbusCreditScreen.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
  
	-- all display objects must be inserted into group
	group:insert( background )
  local scale = 0.8
  toggle.createWithSizeNoValue("BackButton", "BackButtonPressed", 90, 50, 
              201 * scale, 162 * scale, group,
    function( t ) 
      timer.performWithDelay(400, function() storyboard.gotoScene( "menu", "crossFade", 500 ) end)      
    end
  )
  
  local buttonScale = 0.5
  local buttonX, buttonY = 510, 600
  on = createImage("MusicSwitchOn", buttonX, buttonY, 318 * buttonScale, 256 * buttonScale, group)  
  off = createImage("MusicSwitchOff", buttonX, buttonY, 318 * buttonScale, 256 * buttonScale, group)  
  off.isVisible = false
  on:addEventListener("tap", musicOff)
  off:addEventListener("tap", musicOn)
  
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	

end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene