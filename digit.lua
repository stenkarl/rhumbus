this = {}

local w = 47
local scale = 0.4

local options =
{
    width = w,
    height = 67,
    numFrames = 10,
    sheetContentWidth = 470,
    sheetContentHeight = 67 --64
}
local sheet = graphics.newImageSheet( "images/LevelPointsNumberStrip.png", options )

local function makeDigit( num )
  local image = display.newImage(sheet, num+1)
  image:setReferencePoint( display.TopLeftReferencePoint )  
  image.xScale, image.yScale = scale, scale
  return image
end

local function numDigits ( num )
  return string.len(tostring(num))
end

this.makeNumber = function( num )
  local g = display.newGroup()
  local digits = numDigits( num )
  --print ("d "..digits)
  --local padding = 8
  for i = 1,digits do
    local placeValue = 10 ^ (i)
  --print ("num "..num.." pv "..placeValue..", mod "..(num % placeValue))
    local d = makeDigit((num % placeValue) / (10 ^ (i-1)))
    d.x = (digits - i) * (w * scale) --+ (padding * (i-1))
    g:insert(d)
  end
  
  return g
end

return this