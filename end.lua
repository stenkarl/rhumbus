local toggle = require ("toggle")
local numbers = require("numbers")
local storyboard = require("storyboard")

this = {}

local topY = 450
local bottomY = 850
local g

local nextLevel

this.rise = function(nextLevel)
  transition.to(g, {time=2000, y=topY})  
end

local function retract()
  transition.to(g, {time=1500, y=bottomY})  
end

local function restart()
  retract()
  numbers.dismissAll()
  nextLevel()
end

local function quit()
  timer.performWithDelay(400, function() storyboard.gotoScene( "menu", "crossFade", 500 ) end)
end

this.init = function( group, nextLevelCB )
  nextLevel = nextLevelCB
  local image = display.newImageRect("images/GameOver.png", 450, 330)
  image:setReferencePoint( display.TopCenterReferencePoint )
  
  --  image.x, image.y = 420, bottomY
  
  g = display.newGroup()
  --g:setReferencePoint( display.TopCenterReferencePoint )
  g.x, g.y = 423, bottomY  
  g:insert(image)
  
  local scale = 0.5
  toggle.createWithSizeNoValue("ReplayButton", "ReplayPressed", -70, -5, 198 * scale , 138 * scale, g, restart)
  toggle.createWithSizeNoValue("QuitButton", "QuitButtonPressed", 70, -5, 198 * scale , 138 * scale, g, quit)
  
  group:insert(g)
end

return this