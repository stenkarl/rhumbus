local storyboard = require( "storyboard" )
local toggle = require ("toggle")
local slot = require ("slot")
local coins = require("coins")
local stack = require("stack")
local gears = require("gears")
local numbers = require("numbers")
local clear = require("clear")
local gauge = require("gauge")
local levelEnd = require("end")
local levelComplete = require("complete")
local level = require("level")
local gameCenter = require( "gamecenter" )
local particles = require( "particles" )
local scene = storyboard.newScene()
local ops = {}

local levelInProgress = false
local betweenLevels = false

local function enableOps(enable)
  for index,op in ipairs(ops) do
    op.active = enable
  end
end

local function submitHighScore()
	gameCenter.submitScore(level.getScore())
end

local function newLevel()
  if levelInProgress or betweenLevels then return end
  levelInProgress = true
  enableOps(true)
  stack.addCoin()
  coins.retract()
end

local function restart()
  betweenLevels = false
  newLevel()
end

local function nextLevel()
  betweenLevels = false
  stack.reset()

  newLevel()
  
  level.nextLevel()
end

local function gameOver()
  levelInProgress = false
  betweenLevels = true
  numbers.deactivateAll()
  enableOps(false)
  
  levelEnd.rise()
  stack.reset()
  level.reset()
  submitHighScore()
  if not coins.isLit() then
    gameCenter.unlock("Dim", 100)
  end
end

local function doLevelComplete()
  levelInProgress = false
  betweenLevels = true
  numbers.deactivateAll()
  levelComplete.rise()
  if level.getLevel() < 6 then
    gameCenter.unlock("Level5", (level.getLevel() / 5.0) * 100)
  end
  if stack.perfectRound() then
    gameCenter.unlock("Perfect", 100)
  end
  if coins.isGoldOnly() then
    gameCenter.unlock("Gold", 100)
  end
end

local function reset()
  stack.retract()
  slot.reset()
  if coins.levelComplete() then
    doLevelComplete()
  elseif not stack.addCoin() then
    gameOver()
  else 
    enableOps(true)
  end
  
end

local function removeSolutions(solutions, num)
    if num == 3 then
      if stack.removeCoin(solutions[3], reset) then
        slot.showTotal(solutions[num])
      end
    else
      local removeNext = function() removeSolutions(solutions, num + 1) end 
      if stack.removeCoin(solutions[num], removeNext) then 
        if num == 1 then
          slot.showLeft(solutions[num])
        else
          slot.showRight(solutions[num])
        end
      end
    end
end

local function eval()
  enableOps(false)
  numbers.resetNumbers()
  local solutions = slot.eval()
  if stack.hasMatchingCoins(solutions) then
    removeSolutions(solutions, 1)
  else
    slot.showNoMatches(reset)
  end
  --for key,val in pairs(solutions) do
  --  print("solutions "..key..", "..val)
  --end
end

local function createOperators( group )
  local x, y = 233, 663
  local xPad = 115
  local add = toggle.create("AddBrass", "AddBrassPressed", x, y, group, "add", 
                            true, function(t) slot.onSlot(t, 10) end)
  local minus = toggle.create("MinusBrass", "MinusBrassPressed", 
                              x + xPad, y, group, "minus", true, function(t) slot.onSlot(t, 11) end)
  local times = toggle.create("TimesBrass", "TimesBrassPressed", 
                              x + 2 * xPad, y, group, "times", true, function(t) slot.onSlot(t, 12) end)
  local equals = toggle.create("EqualsBrass", "EqualsBrassPressed", 616, 675, group, "equals", true, eval)
  
  ops = {add, minus, times, equals}
end

local function createBackground(name)
  local w = 1367 --iphone
  --local w = 1024
  local background = display.newImageRect( "images/"..name..".png", w,       
    768 )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = -(w-1024)/2, 0 --for iphone
  --background.x, background.y = 0, 0
  return background
end

local function createBack(group)
  local backScale = 0.55
  toggle.createWithSizeNoValue("BackButton", "BackButtonPressed", 60, 32, 
              201 * backScale, 162 * backScale, group,
    function( t ) 
      timer.performWithDelay(400, function() storyboard.gotoScene( "menu", "crossFade", 500 ) end)      
    end
  )  
end

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-- display a background image
	local background = createBackground("NumberRhumbusBkgd1iPhone")  
  local overlay = createBackground("NumberRhumbusBkgd2iPhone")
  local topOverlay = createBackground("NumberRhumbusBkgd3iPhone")

	-- all display objects must be inserted into group
	group:insert( background )
  coins.init(group)
  slot.init(group, toggle, level)

  group:insert(overlay) 
  stack.init(group, gears, gauge, coins, level)

  --createNumbers( group )
  numbers.init( group, slot.onSlot )
  --coins.init(group)
  
  levelEnd.init(group, restart)
  levelComplete.init(group, nextLevel)
  level.init(group)
  coins.initLeftGear(group)

  group:insert(topOverlay)  
  gauge.initialize( group )
  gears.init(group)  
  createBack(group)
  createOperators( group )
  --coins.init(group)
  group:insert(clear.init())
  --level.init(group)
  
  particles.init( group )
end

local function onEnterFrame( event )
  particles.update()
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	newLevel()
  Runtime:addEventListener("enterFrame", onEnterFrame)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
Runtime:removeEventListener("enterFrame", onEnterFrame)
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view

end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene