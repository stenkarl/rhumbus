local gameNetwork = require "gameNetwork"
local json = require "json"

local this = {}
local loggedIntoGC = false

local trophies = { NRTrophyLevel501=false }

local function initCallback( event )
    if event.data then
        loggedIntoGC = true
        --native.showAlert( "Success!", "User has logged into Game Center", { "OK" } )
    else
        loggedIntoGC = false
        --native.showAlert( "Fail", "User is not logged into Game Center", { "OK" } )
    end
end

this.init = function()
	gameNetwork.init( "gamecenter", initCallback )
end

local function requestCallback( event )
    local data = json.encode( event.data )
        
    -- show encoded json string via native alert
   --native.showAlert( "event.data", data, { "OK" } )
end

this.unlock = function( trophy, percent )
	local trophyName = "NRTrophy"..trophy.."01"

	if percent > 100 then
		percent = 100
	end
	
	if trophies[trophyName] then
		--print ("trophy already earned: "..trophyName)
		return
	end
	if percent == 100 then
		trophies[trophyName] = true
	end	
	
	--native.showAlert( "unlocking achievement", trophy, { "OK" } )
	--print ( "unlocking achievement:"..trophyName..", percent "..percent)

	if not loggedIntoGC then return end
		
	gameNetwork.request( "unlockAchievement", {
        achievement = {
            identifier=trophyName,
            percentComplete=percent,
            showsCompletionBanner=true,
        }
        --listener=requestCallback
	})	

end

local function updateTrophies( data )
	for i = 1, #data do
		trophies[data[i].identifier] = data[i].isCompleted
		--if data[i].isCompleted then
		--	native.showAlert("Earned", data[i].identifier, { "OK" })
		--end
	end
end

this.requestTrophies = function( callback )
	if not loggedIntoGC then return end
	
	local function onRequest( event )
		local data = json.encode( event.data )
		
		updateTrophies(event.data)		
        
	    --native.showAlert( "event.data", data, { "OK" } )
	    callback (event.data)
	end
	gameNetwork.request( "loadAchievements", { listener=onRequest } )
	
end

this.submitScore = function( score )
	local boardId = "NRLBHighScore01"
	--print("Submitting score: " ..score.." to "..boardId)
	
	if not loggedIntoGC then return end

	gameNetwork.request( "setHighScore", {
      localPlayerScore = { category=boardId, value=score },
      listener=requestCallback
    })
end

this.reset = function()
	native.showAlert( "resetting achievements", "Achievements Reset", { "OK" } )
	if loggedIntoGC then gameNetwork.request( "resetAchievements" ) end
end

return this