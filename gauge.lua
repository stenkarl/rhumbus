this = {}

local needle

local start = 70
local max = -75
local range = -(max - start)

this.move = function(percent)
  local dest = start - (range * percent)
  transition.to( needle, { time=800, rotation=dest } )
end

this.initialize = function( g )
  local scale = 0.5
  local plateX, plateY = 201 * scale, 343 * scale
  
  local frontPlate = display.newImageRect("images/PressureFrame.png", plateX, plateY)
  frontPlate:setReferencePoint( display.TopCenterReferencePoint )
  frontPlate.x, frontPlate.y = 945, 65
  
  local needleX, needleY = 309 * scale, 43 * scale
  needle = display.newImageRect("images/PressureNeedle.png", needleX, needleY)
  needle.x, needle.y = 900, 150
  needle:rotate(start)
  g:insert(needle)

  g:insert(frontPlate)

end

return this