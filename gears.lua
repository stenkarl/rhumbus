this = {}

local arrow
local gear
local bottomY = 700
local moveTime = 700
local topRotation = 150

local whir = audio.loadSound( "audio/60750__spiderjerusalem__polaroid2.wav" )
local retract = audio.loadSound( "audio/whir.mp3" )

local function doMove(position, after)
  local delta = position - arrow.y
  local degrees = -(delta / bottomY) * topRotation
  transition.to( arrow, { time=moveTime, y=position } )
  transition.to( gear, { time=moveTime, rotation=degrees, onComplete=after })  
end

this.moveTo = function(position, callback)
  audio.play(whir)
  doMove(position, callback)
end

this.moveDown = function()
  if arrow.y == bottomY then
    return
  end
  audio.play(retract)
  doMove(bottomY)  
end

this.init = function( group )
  
  local scale = 0.5
  local arrowW, arrowH = 309 * scale, 163 * scale
  arrow = display.newImageRect("images/RedArrow.png", arrowW, arrowH)
  arrow.x, arrow.y = 850, 700
  
  local gearScale = 0.5
  local gearW, gearH = 634 * gearScale, 626 * gearScale
  gear = display.newImageRect("images/Gear1.png", gearW, gearH)
  gear.x, gear.y = 1000, 700
  
  local barScale = 0.52
  local barW, barH = 40 * barScale, 1536 * barScale
  local bar = display.newImageRect("images/Bar.png", barW, barH)
  bar.x, bar.y = 860, 380
  group:insert(gear)
  group:insert(bar)
  group:insert(arrow)  
  
end

return this