this = {}

local digit = require( "digit" )

local chainLost = audio.loadSound( "audio/LostChain.mp3" )

local image
local group

local levelImg
local scoreImg
local score = 0
local levelNum = 1
local rightEdge = 1020
local multiplierInitialY = 250
local multiplierTargetY = 340
local twox
local threex

local multiplier = 1

local chain = {}
local currentChain = 0

local function initChain()
  local scale = 0.6
  for i = 1,10 do
    chain[i] = display.newImageRect("images/Bonus"..i..".png", 222 * scale, 146 * scale)
    chain[i]:setReferencePoint( display.TopLeftReferencePoint )
    chain[i].x, chain[i].y = 870, multiplierInitialY 
    chain[i].alpha = 0
    group:insert(chain[i])
  end  
end

local function updateLevel( num )
  if levelImg then
    group:remove(levelImg)
  end
  levelImg = digit.makeNumber(num)
  levelImg.x, levelImg.y = rightEdge - levelImg.width, 270
  group:insert(levelImg)
end

local function updateScore( num )
  score = num
  if scoreImg then
    group:remove(scoreImg)
  end
  scoreImg = digit.makeNumber(num)
  scoreImg.x, scoreImg.y = rightEdge - scoreImg.width, 315
  group:insert(scoreImg)
end

local function countToScore( num, counter )
  if counter < num then
    updateScore( score + 1 )
    timer.performWithDelay(50, function() countToScore(num, counter+1) end)
  end
end

this.nextLevel = function()
  levelNum = levelNum + 1
  updateLevel( levelNum )
end

this.addToScore = function( num )
  local addTo = num * multiplier
  multiplier = 1
  countToScore( addTo, 0 )
end

this.reset = function()
  updateLevel( 1 )
  updateScore( 0 )
end

local function hide2x()
  transition.to(twox, 
    {time=200, y=multiplierInitialY, delay=1600, onComplete=function() twox.alpha=0 end}) 
end

this.show2x = function()
  twox.alpha = 1
  multiplier = 2
  transition.to(twox, {time=300, y=multiplierTargetY, onComplete=hide2x})  
end

local function hide3x()
  transition.to(threex, 
      {time=200, y=multiplierInitialY, delay=1600, onComplete=function() threex.alpha=0 end}) 
end

this.show3x = function()
  threex.alpha = 1
  multiplier = 3
  transition.to(threex, {time=300, y=multiplierTargetY, onComplete=hide3x})  
end

local function hideChain(num)
  transition.to(chain[num], {time=300, y=multiplierInitialY, onComplete=function() chain[num].alpha=0 end})
end

local function showChain(num)
  chain[num].alpha = 1
  multiplier = num / 2
  if multiplier < 1 then
    multiplier = 1
  end
  transition.to(chain[num], {time=350, y=350})    
end

this.showNextChain = function()
  if currentChain == 10 then
    multiplier = 5
    return
  end
  currentChain = currentChain + 1
  if currentChain > 2 then
    hideChain(currentChain-1)
  end
  if currentChain > 1 then
    showChain(currentChain)
  end
end

this.resetChain = function()
  if currentChain > 1 then
    audio.play(chainLost)
    hideChain(currentChain)
  end
  currentChain = 0
end

local function initX(which)
  local scale = 0.6
  local img = display.newImageRect( "images/"..which.."BonusSign.png", 222 * scale, 146 * scale)
  img:setReferencePoint( display.TopLeftReferencePoint )  
  img.x, img.y = 875, multiplierInitialY  
  img.alpha = 0
  
  group:insert(img)
  
  return img
end

this.init = function( g )
  group = g
  local scale = 0.5
  image = display.newImageRect( "images/LevelPointsWindow.png", 328 * scale, 236 * scale)
	image:setReferencePoint( display.TopLeftReferencePoint )  
  image.x, image.y = 890, 240
  
  twox = initX("2x")
  threex = initX("3x")
  
  initChain()
  
  group:insert(image)
  updateLevel( 1 )
  updateScore( 0 )
  
end

this.getScore = function()
  return score
end

this.getLevel = function()
  return levelNum
end

return this