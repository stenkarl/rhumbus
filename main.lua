local gameCenter = require "gamecenter"
 
-- function to listen for system events
local function onSystemEvent( event ) 
    if event.type == "applicationStart" then
        gameCenter.init()
        return true
    end
end
Runtime:addEventListener( "system", onSystemEvent )
-- hide the status bar
display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "storyboard" module
local storyboard = require "storyboard"

--print( "scale factor "..(display.pixelWidth / display.actualContentWidth) )

-- load menu screen
storyboard.gotoScene( "game" )
--storyboard.gotoScene( "creditscreen" )
--storyboard.gotoScene("splashscreen")