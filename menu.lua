local storyboard = require( "storyboard" )
local toggle = require("toggle")
local scene = storyboard.newScene()
local gameCenter = require( "gamecenter" )
local background
local loading

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-- display a background image
	background = 
    display.newImageRect( "images/TitleScreenWithGlows.png", display.contentWidth, display.contentHeight )
	background:setReferencePoint( display.TopLeftReferencePoint )
	background.x, background.y = 0, 0
  
	-- all display objects must be inserted into group
	group:insert( background )

  toggle.createWithSizeNoValue("TutorialTitleButton", "TutorialTitleButtonPressed", 120, 520, 
              485, 138, group,
    function( t ) 
      --gameCenter.reset()      
      timer.performWithDelay(400, function() storyboard.gotoScene( "tutorial1", "crossFade", 500 ) end)   
    end
  )
  
  toggle.createWithSizeNoValue("PlayTitleButtons", "PlayTitleButtonPressed", 520, 690, 
              242, 432, group,
    function( t ) 
      loading.alpha = 1
      timer.performWithDelay(400, function() storyboard.gotoScene( "game", "crossFade", 500 ) end)      
    end
  )
  
  toggle.createWithSizeNoValue("CreditsTitleButton", "CreditsTitleButtonPressed", 900, 520, 
              485, 138, group,
    function( t ) 
      	timer.performWithDelay(400, function() storyboard.gotoScene( "creditscreen", "crossFade", 500 ) end)            
    end
  )  
  
  local loadingScale = 0.5
  loading = display.newImageRect( "images/NumberRhumbusLoading.png", 416 * loadingScale, 
          93 * loadingScale)
  loading.x, loading.y = 530, 450
  loading.alpha = 0
  group:insert(loading)

end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. start timers, load audio, start listeners, etc.)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-- INSERT code here (e.g. stop timers, remove listenets, unload sounds, etc.)
	loading.alpha = 0
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	local group = self.view
	
	background:removeSelf()
	background = nil
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene