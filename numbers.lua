this = {}

local toggle = require("toggle")
local numbers = {}

local count = 0
local initialY = 630

local function summon(g, delay)
  transition.to( g, { time=1200, delay=delay, xScale=1, yScale=1, y=g.targetY, transition=easing.outExpo})  
end

local function createRandomNumber()
  return math.random(9)
  
  --count = count + 1
  --if count > 9 then count = 1 end
  --return count
end

local function createArmImage(name, group, x, y, w, h )
  local scale = 0.8
  local img = display.newImageRect("images/"..name..".png", w * scale, h * scale)
  --img.x, img.y = x, y
  img.y = y
  group:insert(img)
  return img
end

local function createArm(index, group, x)
  if index == 0 then
    return createArmImage("ButtonBarRight", group, x + 60, 230, 67,635)
  elseif index == 1 then
    return createArmImage("ButtonBarLeft", group, x, 130, 68, 390)
  end
  return createArmImage("ButtonBarMiddle", group, x + 20, 100, 15, 173)
end

local function createNumber(g, delay)
  local value = createRandomNumber()
  local name = value.."Brass"
  local on, off = toggle.create(name, name.."Pressed", 0, 0, g, value, false,
    function( t ) 
      g.onSlot(t, value)
    end
  )
  g.on, g.off = on, off 
  
  summon(g, delay)
end

local function summonAll()
  local delay = 800
  for index,toggle in ipairs(numbers) do
    
  end
  delay = delay + 100
end

this.init = function( group, onSlot )  
  local x, y = 240, 280
  local xPad, yPad = 120, 120
  local delay = 1000
  for i=0,2 do 
    for j=0,3 do
      local g = display.newGroup()
      g.x, g.y = x + (j * xPad), initialY
      g.targetY = y + (i * yPad)
      g.onSlot = onSlot

      createArm(i, g, x + (j * xPad - 25))

      group:insert(g)

      table.insert(numbers, g)
      createNumber(g, delay)
      delay = delay + 100
    end
  end
  
end

local function remove( g )
  g.on:removeSelf()
  g.off:removeSelf()
  g.on = nil
  g.off = nil
  
  createNumber(g, 100)
end

local function dismiss( toggle, delay )
  transition.to( toggle, { time=700, delay=delay, xScale=0.7, yScale=0.7, y=630, transition=easing.inExpo,
                  onComplete = function() remove(toggle) end})
end

this.resetNumbers = function()
  local delay = 0
  for index,toggle in ipairs(numbers) do
    if toggle.on.isVisible then
      dismiss(toggle, delay)
      delay = delay + 250
    end
  end  
end

this.dismissAll = function()
  local delay = 0
  for index,toggle in ipairs(numbers) do
    dismiss(toggle, delay)
    delay = delay + 100
  end  
end

this.deactivateAll = function()
  local delay = 0
  for index,toggle in ipairs(numbers) do
    toggle.on.active = false
  end  
end

return this