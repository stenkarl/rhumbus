this = {}

local particles = require( "lib_particle_candy" )

this.init = function( group )
	particles.CreateEmitter("E1", 33, 420, 0, false, true)  
  particles.CreateEmitter("E2", 64, 105, 0, false, false)  
  group:insert(particles.GetEmitter("E1"))
  group:insert(particles.GetEmitter("E2"))
  
  
	particles.CreateParticleType ("Smoke", 
		{
		imagePath         = "gfx_particles/smoke3.png",
		imageWidth        = 256 / 4,
		imageHeight       = 256 / 4,
		velocityStart     = 100,	
		velocityVariation = 50,
		velocityChange    = -2,
		weight            = -.15,
		alphaStart        = 0,	
		alphaVariation    = 0.0,
		fadeInSpeed       = .35,
		fadeOutSpeed      = -.25,
		fadeOutDelay      = 1000,
		scaleStart        = .4,	
		scaleVariation    = .25,
		scaleInSpeed      = .35,
		rotationVariation = 360,	
		rotationChange    = 20,	
		emissionShape     = 2,	
		emissionRadius    = 10,	
		killOutsideScreen = false,
		lifeTime          = 3000,
		blendMode         = "screen",
		fxID              = 1,
		} )  

	particles.CreateParticleType ("Flash", 
		{
		imagePath          = "gfx_particles/flare.png",
		imageWidth         = 128 / 2,
		imageHeight        = 128 / 2,
		directionVariation = 360,
		rotationVariation  = 360,
		rotationChange     = 5,
		useEmitterRotation = false,
		alphaStart         = 0.0,
		fadeInSpeed        = 1.0,
		fadeOutSpeed       = -1.0,
		fadeOutDelay       = 1000,
		scaleStart         = 1,
		scaleVariation     = 1,
		killOutsideScreen  = false,
		emissionShape      = 2,		
		emissionRadius     = 5,
		blendMode          = "add",
		lifeTime           = 3000,
		} )

	particles.AttachParticleType("E1", "Smoke", 8, 99999,0) 
  particles.AttachParticleType("E2", "Flash", 1, 500,0) 

	particles.StartEmitter("E1")
  --particles.StartEmitter("E2")
end

this.update = function()
  if math.random(50) == 1 then particles.StartEmitter("E2", true) end
  particles.Update()
end

return this