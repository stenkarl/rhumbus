this = {}

local gameCenter = require( "gamecenter" )
local toggle

local numSlots = 5
local slots = {}
local group
local scale = 0.5
local opW, opH = 81 * scale, 185 * scale
local numW, numH = 141 * scale, 250 * scale
local initialY = 190
local left = 110
local numDistance = 120
local opDistance = 75
local slotOffsets = {0, opDistance, numDistance, opDistance + numDistance, numDistance * 2}
local width, height = 680, 226

local images = {}
local darkNums = {}
local answers = {}
local noMatches
local noMatchY = -50

local slotNum = 1
local numAnswers = 0
local level

local function nextSlot()
  slotNum = slotNum + 1
  local updated = true
  if slotNum > numSlots then
    slotNum = numSlots
    updated = false
  end
  
  return updated
end

local function hideAnswer(solution)
  transition.to(answers[solution], {time=150, y=initialY, delay=1100, 
      onComplete=function() answers[solution].alpha=0 end})    
end

local function showAnswer(solution, cb)
  if numAnswers == 2 then
    level.show2x()
  elseif numAnswers == 3 then
    level.show3x()
  end
  answers[solution].alpha = 1
  transition.to(answers[solution], {time=200, y=75, 
              onComplete=function() hideAnswer(solution); if cb then cb() end end})  
end

local function noMatchesUp(cb)
  transition.to(noMatches, {time=200, y=noMatchY, delay=600, onComplete=cb})  
end
 
this.showNoMatches = function(cb)
  transition.to(noMatches, {time=300, y=40, onComplete=function() noMatchesUp(cb) end})  
end

local function remove(num)
  slots[num] = nil
end

local function isSlotNumeric(num)
  return num % 2 == 1  
end

local function slotNumIsNumeric()
  return isSlotNumeric(slotNum)
end

local function updateSlotNum( value )
  local updated = true
  if value < 10 then
    if not slotNumIsNumeric() then
      updated = nextSlot()
    end
  elseif slots[1] == nil then
    updated = false
  elseif slotNumIsNumeric() then
    updated = nextSlot()
  end
  return updated
end

local function createImage(name, group, w, h)
  local img = display.newImageRect("images/"..name.."Popup.png", w, h)
  img:setReferencePoint( display.TopLeftReferencePoint )
  img.value = name
  img.y = initialY
  img.alpha = 0
  group:insert(img)  
  
  return img
end

local function createDarkImage(name, group, w, h)
  local img = display.newImageRect("images/0"..name.."PopupDark.png", w, h)
  img:setReferencePoint( display.TopLeftReferencePoint )
  img.value = name
  img.y = initialY
  img.alpha = 0
  group:insert(img)  
  
  return img
end

local function createDarkOpImage(name, group)
  local img = display.newImageRect("images/"..name.."PopupDark.png", opW, opH)
  img:setReferencePoint( display.TopLeftReferencePoint )
  img.value = name
  img.y = initialY
  img.alpha = 0
  group:insert(img)  
  
  return img
end

local function createOpImage(name, group)
  return createImage(name, group, opW, opH)
end

local function createNumImage(name, group)
  return createImage(name, group, numW, numH)
end

local function createDarkNumImage(name, group)
  return createDarkImage(name, group, numW, numH)
end

local function initAnswers()
  for i = 1,29 do
    local name = i < 10 and "0"..i or i
    answers[i] = display.newImageRect("images/"..name.."AnswerNumbers.png", 106, 125)
    answers[i]:setReferencePoint( display.TopLeftReferencePoint )
    answers[i].x, answers[i].y = left + 440, initialY
    answers[i].alpha = 0
    group:insert(answers[i])
  end
end

local function initOps()
  images[2] = {}
  images[2][10] = createOpImage("Add", group)
  images[2][11] = createOpImage("Minus", group)
  images[2][12] = createOpImage("Times", group)  
  
  images[4] = {}
  images[4][10] = createOpImage("Add", group)
  images[4][11] = createOpImage("Minus", group)
  images[4][12] = createOpImage("Times", group)  
  
  darkNums[2] = {}
  darkNums[2][10] = createDarkOpImage("Add", group)
  darkNums[2][11] = createDarkOpImage("Minus", group)
  darkNums[2][12] = createDarkOpImage("Times", group)
  
  darkNums[4] = {}
  darkNums[4][10] = createDarkOpImage("Add", group)
  darkNums[4][11] = createDarkOpImage("Minus", group)
  darkNums[4][12] = createDarkOpImage("Times", group)  

end

local function initNums()
  for j = 1,5,2 do
    images[j] = {}
    darkNums[j] = {}
    for i =1,9 do
      images[j][i] = createNumImage(i, group)
      
      darkNums[j][i] = createDarkNumImage(i, group)     
    end  
  end
end

local function initNoMatches()
  noMatches = display.newImageRect("images/NoMatch.png", 400, 123)
  noMatches:setReferencePoint( display.TopLeftReferencePoint )
  noMatches.x, noMatches.y = left + 125, noMatchY
  
  group:insert(noMatches)
end

this.init = function(g, t, lvl)
  group = g
  toggle = t
  level = lvl
  
  initNums()
  initOps()
  initNoMatches()
  initAnswers()
end

local function getTargetSlotHeight(slotNum)
  return isSlotNumeric(slotNum) and 75 or 110
end

local function move(slotNum, up, listener)
  local to = up and getTargetSlotHeight(slotNum) or initialY
  local time = up and 200 or 150
  slots[slotNum].darkNum.y = to
  --print ("image x "..slots[slotNum].image.x)
  --print ("darkNum "..slotNum.." x "..slots[slotNum].darkNum.x.." y "..slots[slotNum].darkNum.y)

  transition.to(slots[slotNum].image, {time=time, y=to, onComplete=listener})
end

local function doSetSlot(slotNum, which, t)
  --print("set slot "..slotNum..","..which.." offset "..slotOffsets[slotNum])
  local img = images[slotNum][which]
  local darkImg = nil
  darkImg = darkNums[slotNum][which]
  darkImg.x = slotOffsets[slotNum] + 230
  darkImg.alpha = 0
  slots[slotNum] = {image=img, darkNum=darkImg, toggle=t}
  img.x = slotOffsets[slotNum] + 230
  img.alpha = 1
  
  move(slotNum, true)  
end

local function doClearSlot(slotNum, listener)
    toggle.flipOff(slots[slotNum].toggle)
    move(slotNum, false, listener)
end

local function setSlot(slotNum, which, t)
  if slots[slotNum] then
    toggle.flipOff(slots[slotNum].toggle)
    move(slotNum, false, function() doSetSlot(slotNum, which, t) end)
  else
    doSetSlot(slotNum, which, t)
  end
  
end

local function clearSlot(slotNum)
  if slots[slotNum] then
    slots[slotNum].isVisible = false
  end
end

local function plus(left, right)
  return left + right
end

local function minus(left, right)
  return left - right
end

local function times(left, right)
  return left * right
end

local function doEval(left, op, right)
  if op.image.value == "Add" then
    return plus(left, right)
  elseif op.image.value == "Minus" then
    return minus(left, right)
  else
    return times(left, right)
  end
  
end

local function getValidSlots()
  local count = 0
  for i = 1,5 do
    if slots[i] then count = count + 1 end
  end
  return count
end

this.eval = function()
  local slotCount = getValidSlots()
  local solutions = {}
  if slotCount >= 3 then
    solutions[1] = doEval(slots[1].image.value, slots[2], slots[3].image.value)
  end
  
  if slotCount >= 5 then
    solutions[2] = doEval(slots[3].image.value, slots[4], slots[5].image.value)
    solutions[3] = doEval(solutions[1], slots[4], slots[5].image.value)
  end
  
  return solutions
end

this.reset = function()
  slotNum = 1
  numAnswers = 0
  for i = 1,5 do
    if slots[i] then 
      doClearSlot(i, function() remove(i) end)
        
    end
  end
end

this.onSlot = function( t, value )
  if t.isVisible then
    if updateSlotNum(value) then
      setSlot(slotNum, value, t)
    end
  else
    clearSlot(slotNum)
  end  
end

local function hideDark(num)
  if slots[num] then
    slots[num].image.alpha = 1
    slots[num].darkNum.alpha = 0  
  end
end

local function showDark(num)
  if slots[num] then
    slots[num].image.alpha = 0
    slots[num].darkNum.alpha = 1  
  end
end

this.showLeft = function(solution)
  numAnswers = numAnswers + 1
  showAnswer(solution)
  showDark(4)
  showDark(5)
end

this.showRight = function(solution)
  numAnswers = numAnswers + 1
  showAnswer(solution)  
  hideDark(4)
  hideDark(5)
  showDark(1)
  showDark(2)
end

this.showTotal = function(solution, cb)
  numAnswers = numAnswers + 1
  showAnswer(solution, cb)  
  
  for i = 1,5 do
    hideDark(i)
  end  
  
  if numAnswers == 3 then
    gameCenter.unlock("Triple", 100)
  end
  numAnswers = 0
end

return this