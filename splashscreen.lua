local storyboard = require( "storyboard" )
local audiomanager = require( "audiomanager" )
local scene = storyboard.newScene()
local background

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view

	-- display a background image
	background = display.newImageRect( "images/HomesteadScreen@4x.png", 1024, 768 )
	background.x = display.contentWidth / 2
	background.y = display.contentHeight / 2	
  
	group:insert( background )
end

local function gotoMenu()
	storyboard.gotoScene( "menu", "fade", 1000 )
end

-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
  audiomanager.on()
	timer.performWithDelay(2000, gotoMenu)
end

-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	
end

-- If scene's view is removed, scene:destroyScene() will be called just prior to:
function scene:destroyScene( event )
	background:removeSelf()
	background = nil
end

-----------------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
-----------------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched whenever before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-----------------------------------------------------------------------------------------

return scene