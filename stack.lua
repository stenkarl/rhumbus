this = {}

local physics = require("physics")
local gameCenter = require( "gamecenter" )

local gauge

local maxVal = 29

local group
local gears
local progress
local level

local coinCount = 1
local bottomY = 700
local maxCoins = 6
local perfect = true

local sounds = {audio.loadSound( "audio/167308__dasdeer__metal-hit-1.wav" )
               -- audio.loadSound( "audio/167308__dasdeer__metal-hit-1.wav" )
  }

-- coins is a table of coin value to coin
local coins = {}

physics.start(); physics.pause()
physics.setScale(80)
physics.setPositionIterations( 16 )
physics.setVelocityIterations( 12 )
--physics.setDrawMode("hybrid")

local function createBound( x, y, w, h )
	local b = display.newRect(x, y, w, h)
	b.isVisible = false
	physics.addBody( b, "static", { friction=0.8 } )
	
	return b
end

local function playRandomSound()
  audio.play(sounds[1])
end

local function initBounds()
  local leftX, rightX = 708, 837
  local thickness = 3
  group:insert(createBound(leftX, 0, thickness, bottomY))
  group:insert(createBound(rightX, 0, thickness, bottomY))
  group:insert(createBound(leftX, bottomY, rightX - leftX, thickness)) 
end

local function getRandomValue()
  local val = math.random(maxVal)
  
  while coins[val] do
    --print("dup "..val)
    val = val + 1
    if val > maxVal then
      val = 1
    end
  end
  
  return val
end

local function createCoin()
  --local name = coinCount
  -- guard against duplicates.
  local coinValue = getRandomValue()
  local name = coinValue
  if name < 10 then
    name = "0"..name
  end
  local coin = display.newImageRect("images/"..name..".png", 125, 124)
  coin.x, coin.y = 775, -50
  coin.value = coinValue
  coinCount = coinCount + 1
	physics.addBody( coin, { density=10, friction=1.0, bounce=0.1, radius=60 } )
  
  local function onCollide( event )
		if event.phase == "began" then
			playRandomSound()
		end
	end
	
	coin:addEventListener("collision", onCollide)  

  coins[coin.value] = coin  
  return coin
end

local function doDismissCoin(coin, callback)
  coins[coin.value] = nil
  coin:toBack()
  physics.removeBody(coin)
  transition.to( coin, { time=200, xScale=0.8, yScale=0.8, transition=easing.inExpo,
               onComplete = 
        function ()    
            transition.to( coin, { time=800, y=bottomY + 10, transition=easing.inExpo,
                  onComplete = function() 
                                  coin:removeSelf()
                                  coin = nil
                                  if callback then callback() end
                               end})  
        end})     
end

local function dismissCoin(value)
  --print("dismissing "..value)
  if coins[value] then
    local c = coins[value]
    coins[value] = nil
    
    --print("coin y"..c.y)
    gears.moveTo(c.y, function() doDismissCoin(c) end)
  end
end

local function printTable(t)
  local n = 0
  for k, v in pairs(t) do
      print ("Item"..n.."= key:"..k)
      n = n + 1
  end

end

local function size(t)
  local n = 0
  for k, v in pairs(t) do
      n = n + 1
  end
  return n
end

local function removeElement(t, coin) 
  for k, v in pairs(t) do
    if v == coin then
      t[k] = nil
      return
    end
  end 
end

local function findAndRemoveMinY(dismissableCoins)
  local minCoinY = nil
  for k, coin in pairs(dismissableCoins) do
    if minCoinY == nil or minCoinY.y > coin.y then
      minCoinY = coin
    end
  end  
  removeElement(dismissableCoins, minCoinY)
  return minCoinY
end

local function percentFull()
  return size(coins) / maxCoins
end

-- Returns true if a coin was added, false if the stack is full.
this.addCoin = function() 
  if percentFull() >= 1 then
    return false
  end
  group:insert(createCoin())
  gauge.move(percentFull())
  
  if size(coins) > 1 then
    perfect = false
    level.resetChain()
  end

  return true
end

local function checkCoinAgainstSolutions(solutions, coin)
  for key,val in pairs(solutions) do
    if coin.value == val then
      --print("checkCoinAgainstSolutions found "..coin.value..", y:"..coin.y)
      return coin
    end
  end  
  return nil
end

function doRemoveCoin(coin, callback)
  if size(coins) == 1 then
    level.showNextChain()
  end
  if percentFull() == 1 then
    gameCenter.unlock("Brink", 100)
  end
  gears.moveTo(coin.y, function() doDismissCoin(coin, 
                                function()
                                  level.addToScore(coin.value)
                                  progress.move(coin.value)
                                  gauge.move(percentFull())
                                  callback()
                                end
                              )
                       end
               )
end

this.isCoinPresent = function(value)
  for key,coin in pairs(coins) do
    if coin.value == value then
      return true
    end
  end
  return false
end

this.removeCoin = function(value, callback)
  local matchingCoin = nil
  for key,coin in pairs(coins) do
    if coin.value == value then
      matchingCoin = coin
      break
    end
  end
  if matchingCoin ~= nil then
    doRemoveCoin(matchingCoin, callback)
    return true
  else
    callback()
    return false
  end
end

this.reset = function()
  perfect = true
  for k, v in pairs(coins) do 
    doDismissCoin(v)
  end
end

this.retract = function()
  gears.moveDown()
  gauge.move(percentFull())  
end

this.hasMatchingCoins = function(solutions)
  for key,coin in pairs(coins) do
    if checkCoinAgainstSolutions(solutions, coin) then
      return true
    end
  end
  return false
end


this.init = function( g, gear, ge, pr, lvl)
  physics.start()
  group = display.newGroup()
  gears = gear
  gauge = ge
  progress = pr
  level = lvl
  
  g:insert(group)
  initBounds()

end

this.perfectRound = function()
  return perfect
end

return this