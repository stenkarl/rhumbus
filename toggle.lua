this = {}

local click = audio.loadSound( "audio/click.wav" )

local function createImage(name, x, y, w, h, group, listener)
	local img = display.newImageRect("images/"..name..".png", w, h)
	img.x, img.y = x, y
	img:addEventListener("tap", listener)
	
	group:insert(img)
	
	return img
end

local function doFlip( toggle )
  if toggle.isVisible then
    toggle:dispatchEvent({name="tap", target=toggle, programmatic=true})
  end  
end

this.flipOff = function( toggle )
  doFlip(toggle)
end

local function remove( toggle )
  toggle[1]:removeSelf()
  toggle[1] = nil
  toggle[2]:removeSelf()
  toggle[2] = nil
end

this.dismiss = function( toggle )
  if toggle[1].isVisible then
    transition.to( toggle[1], { time=800, xScale=0.7, yScale=0.7, y=630, 
                  onComplete = function() remove(toggle) end})
  end
end

local function doCreate( off, on, x, y, w, h, group, value, oneClick, callback)
	local offImage
	local onImage

	local function switch( event )
    if not onImage.active and not event.programmatic then return end
    if onImage.isVisible and not event.programmatic then return end
    
		onImage.isVisible = not onImage.isVisible
		offImage.isVisible = not offImage.isVisible
    audio.play( click )
    
    -- only callback on the 'down' click
    if not oneClick or (oneClick and onImage.isVisible) then
      callback( onImage )
    end
    
    if oneClick and onImage.isVisible then
      timer.performWithDelay(300, function() doFlip(onImage) end)
    end
    
	end
	
	offImage = createImage(off, x, y, w, h, group, switch)
	onImage = createImage(on, x, y, w, h, group, switch)    

  onImage.value = value
	onImage.isVisible = false
  onImage.active = true
	
	return onImage, offImage
end

this.createWithSizeNoValue = function(off, on, x, y, w, h, group, callback)
  return doCreate(off, on, x, y, w, h, group, 0, true, callback)
end

this.createWithSizeNoValueToggle = function(off, on, x, y, w, h, group, callback)
  return doCreate(off, on, x, y, w, h, group, 0, false, callback)
end

this.create = function( off, on, x, y, group, value, oneClick, callback)
  return doCreate(off, on, x, y, 100, 100, group, value, oneClick, callback) 
end

return this